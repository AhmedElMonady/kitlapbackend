﻿using KitLapBackend.Interfaces.IManagers;
using KitLapBackend.Interfaces.IServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Services
{
    public class ImageService : IImageService
    {
        private readonly IImageManager _imageManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ImageService(IImageManager imageManager, IWebHostEnvironment webHostEnvironment)
        {
            _imageManager = imageManager;
            _webHostEnvironment = webHostEnvironment;
        }
        public async Task<HttpStatusCode> UploadImage(IFormFile image, int productId)
        {
            string path = _webHostEnvironment.WebRootPath + @"ImageUploads\";

            _imageManager.InitializeDirectories(path);

            try
            {
                if (image.Length <= 0)
                    return HttpStatusCode.NotFound;

                var imageExtension = System.IO.Path.GetExtension(image.FileName);

                if (!_imageManager.IsValidImage(imageExtension))
                    return HttpStatusCode.BadRequest;

                string fileName = _imageManager.GenerateImageFileName(imageExtension);

                var status = await _imageManager.SaveImageToFile(path, fileName, image, productId);

                return status;
            }
            catch (Exception ex)
            {

                return HttpStatusCode.BadRequest;
            }
        }
    }
}
