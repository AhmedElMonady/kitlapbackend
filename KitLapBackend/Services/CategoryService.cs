﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using KitLapBackend.Data;
using KitLapBackend.DTOs.Requests;
using KitLapBackend.DTOs.Responses;
using KitLapBackend.Interfaces.IManagers;
using KitLapBackend.Interfaces.IServices;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Services
{
    public class CategoryService: ICategoryService
    {
        private readonly ICategoryManager _categoryManager;
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CategoryService(ICategoryManager categoryManager, DataContext context, IMapper mapper)
        {
            _categoryManager = categoryManager;
            _context = context;
            _mapper = mapper;
        }

        public async Task<HttpStatusCode> AddCategory(AddCategoryDto addCategoryDto)
        {
            //Check for duplicate Category Name.
            if (await _categoryManager.CheckCategoryExistence(addCategoryDto.CategoryName) == true)
                return HttpStatusCode.BadRequest;

            var status = await _categoryManager.AddCategory(addCategoryDto);

            return status > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }

        public async Task<HttpStatusCode> DeleteAllCategories()
        {
            var exist = await _categoryManager.CheckAllCategoriesExistence();

            if(!exist)
                return HttpStatusCode.NotFound;

            _context.Categories.RemoveRange(await _context.Categories.ToListAsync());

            await _context.SaveChangesAsync();

            return HttpStatusCode.OK;   
        }

        public async Task<HttpStatusCode> DeleteCategory(DeleteCategoryDto deleteCategoryDto)
        {
            var category = await _context.Categories.FirstOrDefaultAsync(category => category.Id == deleteCategoryDto.CategoryId);

            if (category == null)
                return HttpStatusCode.NotFound;

            _context.Categories.Remove(category);

            return await _context.SaveChangesAsync() > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }

        public async Task<List<CategoryDto>> GetAllCategories()
        {
            var exist = await _categoryManager.CheckAllCategoriesExistence(); 
            
            if (!exist)
                return null;

            var categories = await _context.Categories.ProjectTo<CategoryDto>(_mapper.ConfigurationProvider).ToListAsync();
            
            return categories;  
        }

        public async Task<CategoryDto> GetCategoryById(GetCategoryByIdDto getCategoryByIdDto)
        {
            var category = await _context.Categories
                .ProjectTo<CategoryDto>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(category => category.Id == getCategoryByIdDto.CategoryId);

            return category;
        }

        public async Task<CategoryDto> GetCategoryByName(GetCategoryByNameDto getCategoryByNameDto)
        {
            var category = await _context.Categories
                .ProjectTo<CategoryDto>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(category => category.CategoryName == getCategoryByNameDto.CategoryName);

            return category;
        }

        public async Task<HttpStatusCode> UpdateCategoryName(UpdateCategoryDto updateCategoryDto)
        {
            var updateCategory = await _context.Categories.FirstOrDefaultAsync(category => category.Id == updateCategoryDto.CategoryId);

            if (updateCategory == null)
                return HttpStatusCode.NotFound;

            updateCategory.CategoryName = updateCategoryDto.UpdatedCategoryName;

            _context.Categories.Update(updateCategory);

            return await _context.SaveChangesAsync() > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }
    }
}
