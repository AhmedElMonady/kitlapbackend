﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using KitLapBackend.Data;
using KitLapBackend.DTOs.Requests;
using KitLapBackend.DTOs.Responses;
using KitLapBackend.Interfaces.IServices;
using KitLapBackend.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Services
{
    public class ProductService : IProductService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IImageService _imageService;

        public ProductService(DataContext context, IMapper mapper, IImageService imageService)
        {
            _context = context;
            _mapper = mapper;
            _imageService = imageService;
        }
        public async Task<HttpStatusCode> AddNewProduct(AddProductDto productDto)
        {
            var product = new Product
            {
                Title = productDto.Title,
                Description = productDto.Description,
                Price = productDto.Price,
                HasDiscount = productDto.HasDiscount,
                DiscountRate = productDto.DiscountRate,
                Ratings = new List<Rating>(),
                //Categories = new List<Category>()
            };

            await _context.Products.AddAsync(product);

            var saveStatus = await _context.SaveChangesAsync();

            var uploadStatus = await _imageService.UploadImage(productDto.Image, product.Id);

            if(uploadStatus == HttpStatusCode.OK && saveStatus > 0)
                return HttpStatusCode.OK;

            return HttpStatusCode.BadRequest;
        }

        public async Task<HttpStatusCode> DeleteAllProducts()
        {
            if (!await _context.Products.AnyAsync())
                return HttpStatusCode.NotFound;

            _context.Products.RemoveRange(await _context.Products.ToListAsync());

            return await _context.SaveChangesAsync() > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }

        public async Task<HttpStatusCode> DeleteProduct(DeleteProductDto productDto)
        {
            var product = await _context.Products.FirstOrDefaultAsync(product => product.Id == productDto.ProductId);
            
            if (product == null)
                return HttpStatusCode.NotFound;

            _context.Products.Remove(product);

            return await _context.SaveChangesAsync() > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }

        public async Task<List<ProductDetailsDto>> GetAllProductsDetails()
        {
            if (!await _context.Products.AnyAsync())
                return null;

            var list = await _context.Products.Include(c => c.Categories)
                .Include(r => r.Ratings)
                .ProjectTo<ProductDetailsDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return list;
        }

        public async Task<List<ProductSummaryDto>> GetAllProductsSummary()
        {
            if (!await _context.Products.AnyAsync())
                return null;

            var list = await _context.Products.Include(c => c.Categories)
                  .Include(r => r.Ratings)
                  .ProjectTo<ProductSummaryDto>(_mapper.ConfigurationProvider)
                  .ToListAsync();

              return list;
            return null;
        }

        public async Task<ProductDetailsDto> GetProductDetails(GetProductDto productDto)
        {
            var product = await _context.Products/*.Include(c => c.Categories)*/
                 .Include(r => r.Ratings).ProjectTo<ProductDetailsDto>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync(product => product.Id == productDto.ProductId);

             return product;
            return null;
        }

        public async Task<ProductSummaryDto> GetProductSummary(GetProductDto productDto)
        {
              var product = await _context.Products.Include(c => c.Categories)
                  .Include(r => r.Ratings).ProjectTo<ProductSummaryDto>(_mapper.ConfigurationProvider)
                  .FirstOrDefaultAsync(product => product.Id == productDto.ProductId);

              return product;
            return null;
        }

        public async Task<HttpStatusCode> UpdateProduct(UpdateProductDto productDto)
        {
            var product = await _context.Products.FirstOrDefaultAsync(product => product.Id == productDto.ProductId);

            if (product == null)
                return HttpStatusCode.NotFound;

            product.Title = productDto.Title;
            product.Description = productDto.Description;
            //product.ImageUrl = productDto.ImageUrl;
            product.Price = productDto.Price;
            product.HasDiscount = productDto.HasDiscount;
            product.DiscountRate = productDto.DiscountRate;

            _context.Products.Update(product);

            return await _context.SaveChangesAsync() > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }
    }
}
