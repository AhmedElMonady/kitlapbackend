﻿using KitLapBackend.Data;
using KitLapBackend.DTOs.Requests;
using KitLapBackend.DTOs.Responses;
using KitLapBackend.Interfaces.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : BaseController
    {
        private readonly DataContext _context;
        private readonly IProductService _productService;

        public ProductsController(DataContext context, IProductService productService)
        {
            _context = context;
            _productService = productService;
        }

        /// <summary>
        /// Adds A new product with its own image and details from a FormBody
        /// </summary>
        /// <param name="productDto"></param>
        /// <returns>Ok if product is added ssuccessfully and BadRequest if it fails to.</returns>
        [HttpPost, Route("AddProduct")]
        public async Task<ActionResult> AddProduct([FromForm] AddProductDto productDto)
        {
            if (productDto == null)
                return BadRequest("Product Cannot be Null");

            if (productDto.HasDiscount && productDto.DiscountRate == 0)
                return BadRequest("Discount Rate MUST be set");

            var status = await _productService.AddNewProduct(productDto);

            if (status == HttpStatusCode.BadRequest)
                return BadRequest("Failed to Create new Product");

            return Ok("Product Created Successfully");
        }

        [HttpPost, Route("UpdateProduct")]
        public async Task<ActionResult> UpdateProduct(UpdateProductDto productDto)
        {
            if (productDto == null)
                return BadRequest("Product Cannot be Null");

            var status = await _productService.UpdateProduct(productDto);

            if (status == HttpStatusCode.NotFound)
                return NotFound("No such Product Exists");

            if (status == HttpStatusCode.BadRequest)
                return BadRequest("Failed to update the product.");

            return Ok("Product Updated Successfully");
        }

        [HttpPost, Route("GetProductSummary")]
        public async Task<ActionResult<ProductSummaryDto>> GetProductSummary(GetProductDto productDto)
        {
            if (productDto == null)
                return BadRequest("Product/Id Cannot be Null");

            var product = await _productService.GetProductSummary(productDto);

            if (product == null)
                return NotFound("No such Product Exists");

            return Ok(product);
        }

        [HttpPost, Route("GetProductDetails")]
        public async Task<ActionResult<ProductDetailsDto>> GetProductDetails(GetProductDto productDto)
        {
            if (productDto == null)
                return BadRequest("Product/Id Cannot be Null");

            var product = await _productService.GetProductDetails(productDto);

            if (product == null)
                return NotFound("No such Product Exists");

            return Ok(product);
        }

        [HttpGet, Route("GetAllProductsSummary")]
        public async Task<ActionResult<List<ProductSummaryDto>>> GetAllProductsSummary()
        {
            var products = await _productService.GetAllProductsSummary();

            if (products == null)
                return NotFound("No Products Exist");

            return Ok(products);
        }

        [HttpGet, Route("GetAllProductsDetails")]
        public async Task<ActionResult<List<ProductDetailsDto>>> GetAllProductsDetails()
        {

            var products = await _productService.GetAllProductsDetails();

            if (products == null)
                return NotFound("No Products Exist");

            return Ok(products);
        }

        [HttpDelete, Route("DeleteProduct")]
        public async Task<ActionResult> DeleteProduct(DeleteProductDto productDto)
        {
            if (productDto == null)
                return BadRequest("Product Cannot be Null");

            var status = await _productService.DeleteProduct(productDto);

            if (status == HttpStatusCode.NotFound)
                return NotFound("Product Not found");

            if (status == HttpStatusCode.BadRequest)
                return BadRequest("Failed to delete product");

            return Ok("Product Deleted Successfully!");
        }

        [HttpDelete, Route("DeleteAllProducts")]
        public async Task<ActionResult> DeleteAllProducts()
        {
            if (!await _context.Products.AnyAsync())
                return NotFound("No Products Exist");

            var status = await _productService.DeleteAllProducts();

            if (status == HttpStatusCode.BadRequest)
                return BadRequest("Failed to Delete all products.");

            return Ok("Products Deleted Successfully!");
        }

        public class LinkCategoryDto
        {
            public int PID { get; set; }
            public int CID { get; set; }
        }

        public class DelinkCategoryDto
        {
            public int PID { get; set; }
            public int CID { get; set; }
        }
        [HttpPost, Route("LinkCategory")]
        public async Task<ActionResult> LinkCategory(LinkCategoryDto category)
        {
            var product = await _context.Products.Include(p => p.Categories).FirstOrDefaultAsync(p => p.Id == category.PID);

            var category_ = await _context.Categories.FirstOrDefaultAsync(c => c.Id == category.CID);

            product.Categories.Add(category_);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost, Route("DelinkCategory")]
        public async Task<ActionResult> DelinkCategory(DelinkCategoryDto category)
        {
            var product = await _context.Products.Include(p => p.Categories).FirstOrDefaultAsync(p => p.Id == category.PID);

            var category_ = await _context.Categories.FirstOrDefaultAsync(c => c.Id == category.CID);

            product.Categories.Remove(category_);

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
