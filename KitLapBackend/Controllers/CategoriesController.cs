﻿using KitLapBackend.DTOs.Requests;
using KitLapBackend.DTOs.Responses;
using KitLapBackend.Interfaces.IServices;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : BaseController
    {
        private readonly ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpPost, Route("AddCategory")]
        public async Task<ActionResult> AddCategory(AddCategoryDto addCategoryDto)
        {
            if (addCategoryDto == null)
                return NotFound("Category cannot be null.");

            var status = await _categoryService.AddCategory(addCategoryDto);

            if (status == HttpStatusCode.BadRequest)
                return BadRequest("Failed to add Category (Category may be previously existing in the database).");

            return Ok("Category added successfully.");
        }

        [HttpGet, Route("GetAllCategories")]
        public async Task<ActionResult<List<CategoryDto>>> GetAllCategories()
        {
            var categories = await _categoryService.GetAllCategories();

            if (categories == null)
                return NotFound("No Categories Exist.");

            return Ok(categories);
        }

        [HttpPost, Route("GetCategoryById")]
        public async Task<ActionResult<CategoryDto>> GetCategoryById(GetCategoryByIdDto getCategoryDto)
        {
            if (getCategoryDto == null)
                return NotFound("Object cannot be null.");

            var category = await _categoryService.GetCategoryById(getCategoryDto);

            if (category == null)
                return NotFound("Category does not exist.");

            return Ok(category);
        }  
        
        [HttpPost, Route("GetCategoryByName")]
        public async Task<ActionResult<CategoryDto>> GetCategoryByName(GetCategoryByNameDto getCategoryBynameDto)
        {
            if (getCategoryBynameDto == null)
                return NotFound("Object cannot be null.");

            var category = await _categoryService.GetCategoryByName(getCategoryBynameDto);

            if (category == null)
                return NotFound("Category does not exist.");

            return Ok(category);
        }

        [HttpPost, Route("UpdateCategory")]
        public async Task<ActionResult> UpdateCategoryName(UpdateCategoryDto updateCategoryDto)
        {
            if (updateCategoryDto == null)
                return NotFound("Object cannot be null.");

            var updateStatus = await _categoryService.UpdateCategoryName(updateCategoryDto);

            if (updateStatus == HttpStatusCode.NotFound)
                return NotFound("Category Not Found.");

            if (updateStatus == HttpStatusCode.BadRequest)
                return BadRequest("Failed to Update Category Name");

            return Ok("Category Name Updated Successfully!");
        }

        [HttpDelete, Route("DeleteCategory")]
        public async Task<ActionResult> DeleteCategory(DeleteCategoryDto deleteCategoryDto)
        {
            if (deleteCategoryDto == null)
                return BadRequest("Object cannot be null.");

            var deleteStatus = await _categoryService.DeleteCategory(deleteCategoryDto);

            if (deleteStatus == HttpStatusCode.NotFound)
                return NotFound("Category Not Found.");

            if (deleteStatus == HttpStatusCode.BadRequest)
                return BadRequest("Failed to delete Category.");

            return Ok("Category deleted successfully");
        }

        [HttpDelete, Route("DeleteAllCategories")]
        public async Task<ActionResult> DeleteAllCategories()
        {
            var status = await _categoryService.DeleteAllCategories();

            if (status == HttpStatusCode.NotFound)
                return NotFound("No Categories Exist");

            return Ok("Categories Deleted Successfully!");
        }
    }
}
