﻿using KitLapBackend.DTOs.Requests;
using KitLapBackend.Interfaces.IServices;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : BaseController
    {
        private readonly IImageService _imageService;

        public ImagesController(IImageService imageService)
        {
            _imageService = imageService;
        }
        
        [HttpPost, Route("UploadImage")]
        public async Task<ActionResult> UploadImage([FromForm] AddImageDto imageDto)
        {
            if (imageDto == null)
                return BadRequest("Image File cannot be null.");

            var status = await _imageService.UploadImage(imageDto.Image, imageDto.ProductId);

            if (status == HttpStatusCode.NotFound)
                return BadRequest("File is Invalid.");

            if (status == HttpStatusCode.BadRequest)
                return BadRequest("Image type is invalid or Failed to Save Image.");

            return Ok("Saved Image Successfully.");
        }
    }
}
