﻿using Microsoft.AspNetCore.Http;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Interfaces.IManagers
{
    public interface IImageManager
    {
        void InitializeDirectories(string path);
        bool IsValidImage(string fileExtension);
        string GenerateImageFileName(string imageExtension);
        Task<HttpStatusCode> SaveImageToFile(string path, string filename, IFormFile imageDto, int productId);
    }
}
