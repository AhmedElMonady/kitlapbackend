﻿using KitLapBackend.DTOs.Requests;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Interfaces.IManagers
{
    public interface ICategoryManager
    {
        Task<bool> CheckCategoryExistence(string categoryName);
        Task<HttpStatusCode> AddCategory(AddCategoryDto addCategoryDto);
        Task<bool> CheckAllCategoriesExistence();
    }
}
