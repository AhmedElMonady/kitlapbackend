﻿using Microsoft.AspNetCore.Http;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Interfaces.IServices
{
    public interface IImageService
    {
        Task<HttpStatusCode> UploadImage(IFormFile image, int productId);
    }
}
