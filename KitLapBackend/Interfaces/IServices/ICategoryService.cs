﻿using KitLapBackend.DTOs.Requests;
using KitLapBackend.DTOs.Responses;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Interfaces.IServices
{
    public interface ICategoryService
    {
        Task<HttpStatusCode> AddCategory(AddCategoryDto addCategoryDto);
        Task<HttpStatusCode> DeleteAllCategories();
        Task<List<CategoryDto>> GetAllCategories();
        Task<HttpStatusCode> DeleteCategory(DeleteCategoryDto deleteCategoryDto);
        Task<HttpStatusCode> UpdateCategoryName(UpdateCategoryDto updateCategoryDto);
        Task<CategoryDto> GetCategoryById(GetCategoryByIdDto getCategoryByIdDto);
        Task<CategoryDto> GetCategoryByName(GetCategoryByNameDto getCategoryByNameDto);
    }
}
