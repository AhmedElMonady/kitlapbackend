﻿using KitLapBackend.DTOs.Requests;
using KitLapBackend.DTOs.Responses;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Interfaces.IServices
{
    public interface IProductService
    {
        Task<HttpStatusCode> AddNewProduct(AddProductDto productDto);
        Task<HttpStatusCode> UpdateProduct(UpdateProductDto productDto);
        Task<HttpStatusCode> DeleteProduct(DeleteProductDto productDto);
        Task<HttpStatusCode> DeleteAllProducts();
        Task<ProductSummaryDto> GetProductSummary(GetProductDto productDto);
        Task<ProductDetailsDto> GetProductDetails(GetProductDto productDto);
        Task<List<ProductSummaryDto>> GetAllProductsSummary();
        Task<List<ProductDetailsDto>> GetAllProductsDetails();
    }
}
