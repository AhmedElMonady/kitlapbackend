﻿using KitLapBackend.Data;
using KitLapBackend.Interfaces.IManagers;
using KitLapBackend.Models;
using Microsoft.AspNetCore.Http;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Managers
{
    public class ImageManager : IImageManager
    {
        private readonly DataContext _context;

        public ImageManager(DataContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Generates the Image File path name by a random name generator.
        /// </summary>
        /// <param name="imageExtension"></param>
        /// <returns>The generated Image Name appended with its Image Extension.</returns>
        public string GenerateImageFileName(string imageExtension)
        {
            return PasswordGenerator.Generate(50) + imageExtension;
        }

        public void InitializeDirectories(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public bool IsValidImage(string fileExtension)
        {
            if (String.IsNullOrEmpty(fileExtension))
                return false;

            List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".JPE", ".BMP", ".GIF", ".PNG" };

            foreach (var imageExtension in ImageExtensions)
            {
                if (imageExtension.ToLower() == fileExtension.ToLower())
                    return true;
            }
            return false;
        }

        public async Task<HttpStatusCode> SaveImageToFile(string path, string filename, IFormFile image, int productId)
        {
            using (FileStream fs = System.IO.File.Create(path + filename))
            {
                await image.CopyToAsync(fs);
                await fs.FlushAsync();

                await _context.Images.AddAsync(new Image
                {
                    ProductId = productId,
                    ImageUrl = filename
                });

                return await _context.SaveChangesAsync() > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
            }
        }
    }
}
