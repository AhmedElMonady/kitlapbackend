﻿using KitLapBackend.Data;
using KitLapBackend.DTOs.Requests;
using KitLapBackend.Interfaces.IManagers;
using KitLapBackend.Models;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Threading.Tasks;

namespace KitLapBackend.Managers
{
    public class CategoryManager: ICategoryManager
    {
        private readonly DataContext _context;

        public CategoryManager(DataContext context)
        {
            _context = context;
        }

        public async Task<HttpStatusCode> AddCategory(AddCategoryDto addCategoryDto)
        {
            var category = new Category
            {
                CategoryName = addCategoryDto.CategoryName
            };
            _context.Categories.Add(category);

            return await _context.SaveChangesAsync() > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }

        public async Task<bool> CheckAllCategoriesExistence()
        {
            if (!await _context.Categories.AnyAsync())
                return false;
            return true;
        }

        public async Task<bool> CheckCategoryExistence(string categoryName)
        {
            var category = await _context.Categories.
                FirstOrDefaultAsync(category => category.CategoryName == categoryName);

            if (category == null)
                return false;

            return true;
        }
    }
}
