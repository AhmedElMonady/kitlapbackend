﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace KitLapBackend.Models
{
    [Index(nameof(CategoryName), IsUnique = true)]

    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public List<Product> Products { get; set; }
    }
}
