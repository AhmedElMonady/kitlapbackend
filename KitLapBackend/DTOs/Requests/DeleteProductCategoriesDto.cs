﻿namespace KitLapBackend.DTOs.Requests
{
    public class DeleteProductCategoriesDto
    {
        public int ProductId { get; set; }
    }
}
