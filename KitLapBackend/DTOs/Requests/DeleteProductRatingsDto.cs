﻿namespace KitLapBackend.DTOs.Requests
{
    public class DeleteProductRatingsDto
    {
        public int ProductId { get; set; }
    }
}
