﻿using System.ComponentModel.DataAnnotations;

namespace KitLapBackend.DTOs.Requests
{
    public class GetCategoryByNameDto
    {
        [Required]
        public string CategoryName { get; set; }
    }
}
