﻿using System.ComponentModel.DataAnnotations;

namespace KitLapBackend.DTOs.Requests
{
    public class GetCategoryByIdDto
    {
        [Required]
        public int CategoryId { get; set; }
    }
}
