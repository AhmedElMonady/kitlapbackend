﻿using System.ComponentModel.DataAnnotations;

namespace KitLapBackend.DTOs.Requests
{
    public class AddCategoryDto
    {
        [Required]
        public string CategoryName { get; set; }
    }
}
