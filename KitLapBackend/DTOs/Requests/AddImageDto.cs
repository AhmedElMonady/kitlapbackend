﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace KitLapBackend.DTOs.Requests
{
    public class AddImageDto
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public IFormFile Image { get; set; }
    }
}
