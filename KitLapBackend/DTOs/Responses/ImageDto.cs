﻿namespace KitLapBackend.DTOs.Responses
{
    public class ImageDto
    {
        public int Id { get; set; }
        public string  ImageUrl { get; set; }
    }
}
